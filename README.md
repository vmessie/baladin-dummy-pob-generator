# BALAdIN python PoB generator

This program aims at providing a set of tools to test BALAdIN's sawtooth transaction processor, by generating dummy PoBs.
This project also provides a simple API to generate BALAdIN-related transactions

## Dependencies
- Python 3.5+
- [BALAdIN's transaction processor](https://gitlab.com/vmessie/baladin-transaction-processor/)
- [Google protocol buffers](https://developers.google.com/protocol-buffers/docs/overview)
- [PyMongo](http://api.mongodb.com/python/)
- [PyCryptoDome](http://pycryptodome.readthedocs.io/en/latest/)

## Installation

### Sawtooth's Python API

```
git clone https://github.com/hyperledger/sawtooth-core
bin/protogen
cd sawtooth-core/sdk/python
python setup.py build
sudo python setup.py install
```

## Usage
At this time, two test scripts are included:
- `python3 test.py`	(clients/relays addition/deletion test)
- `python3 test_pou.py` (PoB and path management tests)
