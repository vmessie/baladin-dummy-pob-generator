# coding=utf-8
from struct import pack
from random import randint

from Crypto.Hash import SHA256
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15 as Signer


import bson


class FakePoU():
	"""Create a fake Pou

	Parameters
	----------
	keyEmitter : RSA key
		Emitter public key instance (the whole key, not only the public)
	keyReceiver : RSA key
		Receiver public key instance (the whole key, not only the public)
	keyRelays : list
		Relays list of RSA keys instances (the whole keys, not only the public)
	number : int
		The Proof of Use number
	timestamp : int
		The Proof of use timestamp
	dataAmount : int
		Data exchanged since the last PoU
	padding : int
		Next frame
	isFirst : bool
		If the required PoU is for a path creation, should be True (default False: PoU for existing path)

	"""
	def __init__(self, keyEmitter, keyReceiver, keyRelays, number, timestamp, dataAmount, padding,isFirst = False):

		self.keyEmitter 	= keyEmitter
		self.keyReceiver	= keyReceiver
		self.keyRelays		= keyRelays
		self.number			= number
		self.timestamp		= timestamp
		self.dataAmount		= dataAmount
		self.padding		= padding

		self.isFirst = isFirst

		#Génération des nonces aléatoires
		self.nonceEmitter = randint(1, 8e9)
		self.nonceRelays = []

		for k in self.keyRelays:
			self.nonceRelays.append(randint(1, 8e9))
		# Calcul de leur hash
		nonceHashEmitterObj	= SHA256.new()
		nonceHashEmitterObj.update(pack('!Q', self.nonceEmitter))
		self.nonceHashEmitter = nonceHashEmitterObj.digest()

		self.nonceHashRelays = []

		for k in self.nonceRelays:
			obj = SHA256.new()
			obj.update(pack('!Q', k))
			self.nonceHashRelays.append(obj.digest())


		#Calcul de l'ID du lien
		toId = self.keyEmitter.publickey().exportKey('DER')

		for k in self.keyRelays:
			toId += k.publickey().exportKey('DER')

		toId += self.keyReceiver.publickey().exportKey('DER')

		h = SHA256.new(toId)
		pathHash = h.digest()

		self.pathId = pathHash[:16]

		if self.isFirst: self.pathId2Frame = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

		else : self.pathId2Frame = self.pathId

	def calculate(self):
		"""Calculate all the Proof of Use cryptographics values (signatures, ...)
		"""
		# Paquet de base
		pou = pack("!IQ", self.number, self.timestamp) + self.pathId2Frame + pack("!IQ", self.padding, self.dataAmount)

		pou += self.keyEmitter.publickey().exportKey('DER')
		pou += self.nonceHashEmitter

		s = Signer.new(self.keyEmitter)
		h = SHA256.new()
		h.update(pou)
		#Signatures sens aller
		self.sigOutEmitter = s.sign(h)

		pou += self.sigOutEmitter

		self.sigOutRelays = []

		for k in range(0, len(self.keyRelays)):
			n=self.keyRelays[k]
			pou += n.publickey().exportKey('DER')
			pou += self.nonceHashRelays[k]

			s = Signer.new(n)
			h = SHA256.new()
			h.update(pou)
			self.sigOutRelays.append(s.sign(h))

			pou += self.sigOutRelays[-1]


		pou += self.keyReceiver.publickey().exportKey('DER')

		s = Signer.new(self.keyReceiver)
		h = SHA256.new()
		h.update(pou)
		#Le récepteur a reçu et signe à son tour
		self.sigReceiver = s.sign(h)

		pou += self.sigReceiver

		self.sigBackRelays = []
		#Signatures sens retour
		for k in range(len(self.keyRelays)-1, -1, -1):
			n= self.keyRelays[k]
			pou += pack('!Q', self.nonceRelays[k])
			s = Signer.new(n)
			h = SHA256.new()
			h.update(pou)

			self.sigBackRelays.insert(0, s.sign(h))

			pou += self.sigBackRelays[0]

		print(len(self.sigBackRelays))
		print(len(self.keyRelays))

		pou += pack('!Q', self.nonceEmitter)

		s = Signer.new(self.keyEmitter)
		h = SHA256.new()
		h.update(pou)

		self.sigBackEmitter = s.sign(h)

		pou += self.sigBackEmitter


	def toBsonArray(self):
		"""Encode the Proof of Use into an array

		Returns
		-------
		dict
			PoU array representation (compliant to "pou" or "initial_pou" field in BLORK specification)

		"""
		toBson = {
			"path_id" : self.pathId,
			"timestamp" : bson.int64.Int64(self.timestamp),
			"data_amount" : bson.int64.Int64(self.dataAmount),
			"next_frame" : self.padding,
			"number" : self.number,

			"nonces" : [bson.int64.Int64(self.nonceEmitter)],
			"sig_out" : [self.sigOutEmitter],
			"sig_back" : [self.sigBackEmitter],
			"sig_receiver" : bson.binary.Binary(self.sigReceiver)
		}

		for k in range(0, len(self.keyRelays)):
			n=self.keyRelays[k]
			toBson["nonces"].append(bson.int64.Int64(self.nonceRelays[k]))
			toBson["sig_out"].append(self.sigOutRelays[k])
			toBson["sig_back"].append(self.sigBackRelays[k])

		if self.isFirst: del toBson["path_id"]

		return toBson
