from new_client import NewClientTransaction
from new_relay  import NewRelayTransaction
from new_path   import NewPathTransaction
from new_pou	import PoUTransaction
from del_path	import DelPathTransaction

from transaction import BsonTransaction

from Crypto.PublicKey import RSA

from random import randint

from sys import argv

from time import sleep

# Rcupération de l'endpoint + nombre de PoUs + délai

if (len(argv) > 1):endpoint=argv[1]
else:endpoint="http://127.0.0.1:8008"

if (len(argv) > 2):pous = int(argv[2])
else:pous = 10

if (len(argv) > 3):dly = float(argv[3])
else:dly=0.5


# On crée des paires de clés pour l'émetteur / le récepteur
keyEMIT = RSA.generate(2048)
keyRCVS = RSA.generate(2048)

# On les enregistre sur le ledger
ncte = NewClientTransaction(keyEMIT)
ncte.bufferize()
BsonTransaction.sendTx(endpoint)

nctr = NewClientTransaction(keyRCVS)
nctr.bufferize()
BsonTransaction.sendTx(endpoint)

# Nombre aléatoire de relais : entre 2 et 10 relais
nbRelays = randint(2, 10)

print("Nombre de relais à créer : " + str(nbRelays))

keyRLYS = []

# On crée / enregistre sur le registre le nombre de relais requis
for k in range(0, nbRelays):
	keyRLYS.append(RSA.generate(2048))

	nrt = NewRelayTransaction(keyRLYS[-1])
	nrt.bufferize()

# On envoie la transaction de création du circuit
npt = NewPathTransaction(keyEMIT, keyRLYS, keyRCVS)
npt.bufferize()
BsonTransaction.sendTx(endpoint)

print("PoU")
for i in range(1,pous+1):
	pou = PoUTransaction(i, randint(98,110),keyEMIT, keyRLYS, keyRCVS)
	pou.bufferize()
	BsonTransaction.sendTx(endpoint)
	sleep(dly)

# suppression du circuit

dpt = DelPathTransaction(pou.pou.pathId, keyEMIT)
dpt.bufferize()

BsonTransaction.sendTx(endpoint)

