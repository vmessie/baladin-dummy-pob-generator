#!/usr/bin/env python3
# -*- coding: utf-8 -*-


GLOBAL_NAMESPACE="313433"


from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15 as Signer

import bson
import binascii
import base64

from transaction import GLOBAL_NAMESPACE,BsonTransaction



class DelClientTransaction(BsonTransaction):
	def __init__(self, keyEmitter):
		"""Send a del cleient transaction

		Parameters
		----------
		keyEmitter : RSA key
			The key instance of the client to delete
		"""
		BsonTransaction.__init__(self)

		emitterPk 	= keyEmitter.publickey().exportKey('DER')

		addr = GLOBAL_NAMESPACE + "01" + str(binascii.hexlify(emitterPk[-31:]).decode("ascii"))
		print(addr)


		keyIssuer = RSA.import_key(base64.b64decode("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCCSMnvxtwh3RZW" +
							"ny0FK5VuJ07W/ntKjho0JsENp59JjzBrCZRih1dgbdpdaGNgFrbqKgdwQGIt/9wU" +
							"nfnrSsaPFTPzeDfoemqjkV4HOpBmhMu6sWrIcWsTu+CBuchgnL8kNHAbLbSYFrp0" +
							"OnYF41EgKL47GkcnB+/UpjrmwfqnyAdxU41YrRCGGxNS9UUfG5p/R4llOa1XJZZx" +
							"xihL+DAMqYYNwNp+EneyHB2oGoes8b+0akiNGVLmjkMGFxsRrWV8smiFJ5AqnBQ6" +
							"nvSp105pqdVVYpg5B3yE7Yflpb8u8rNVjFiGgiZsjsyFaO4OGgRoEeOI2+Ep8PDO" +
							"rkfHJd1FAgMBAAECggEABGtRpjd5mhB0xIVrsoQy8mSU8Qf6yKGfU+uImtz3szgP" +
							"LWJKzTjIBvZS9N4omMbMd47rZ6xXaDYIkg51vrxvlcTjWTvXsaQNX/T7WoVrBn80" +
							"peWEuL3YIvg5ek0B9fz7ItBAiTt4p7wFoRGmlTaLtuqK4VhX14QWZ4En+FHWHcDH" +
							"1BFrYExrbwnzADTJ0W4syUpHjNf0djDMOFkl5FlF6WBIVcT1l0cSuFhrtjooffHV" +
							"NytnS37e7BdJAK/KZW0ENYuX7XD1ZLFORdLNf5op+pSD49W6UaP9pLKNdmJf0lYF" +
							"Opb0XhZv7K26VzkN0gmX8u5niMNJvchUaZI295uNwQKBgQC3tXPM3/XkLDmvxs0V" +
							"yThT0CUesKAOU8zvugo4yyvdzgYX8f66FSvAaJ/ouxVSjJJWF+bNUP8U8prkCwuC" +
							"n2f5xzt+vDTjlP6TWybMNRpaC+4FWSBpx4QdbgQ6aFXJto9AMSjrG5ZlShK0Dyc+" +
							"/cIDQASMZH2DsqeJa9RU91KpNQKBgQC1jW+melnsuup7Xh4MTUBqVH8GCq5uDa5I" +
							"Ypa2GKy8zsvE7s/nflwQX1lsA0LZrYVllJyIX+XpUKFd1RD/msKGtOlT19ERCzpa" +
							"2vgttOvB9KavH+MKs5REaIl4roEbvJTJyYGaq6nEOw3FNQfeYkov2dFnXD3U/lxh" +
							"/Cv+zxL10QKBgCRgULLDshvhSfbtZZSnyHiIIaGHFdpga5cYfoeqa2A7OGeramYM" +
							"ZSJvVSKvUNSMszlK+E+1dxz9wP/g1AGkcOE7uyMoP0zFJvekt/T9mt8zt4jlkg6A" +
							"DEALldi+6iXL+WeblF+hkdEyrqtqyLmnY7BjD8OA7n2Sdaw3Zs8APiT1AoGBAIuN" +
							"lG0bT1QwuVzrE53RI9qX6Kv0OfBOg9EQN1jxzpWzP9640wbWkl3jbREh0JkSUJ+s" +
							"hEYvsMKPP/qodNCFXVG+bjwMgJ/hSZHdDzfTAgWs0RN8J0FqmGWdFU62lHeFlbkm" +
							"M4F4wp4b5pHYGZxPYUc230fcF4tJmimXdRoAgkqBAoGAVn2pnjbJr1wNFXxml/PT" +
							"yiZuxhZqww/t28NzNb0gUShcx6zQ/CZD/9bxFjw2J86PADjt1ex+U35IYX+SjsfE" +
							"rfzDUjPxGg9dAy31ODDDeu1Evho2QvvrKl5yiySTLjr5TVNTaCROCJfab9g64tZC" +
							"pwu3Vd4NnVx9dyPWC9A9izA="))

		s = Signer.new(keyIssuer)

		h = SHA256.new()
		h.update(emitterPk)

		sigIssuer = s.sign(h)

		self.body = {"client_key":emitterPk,"issuer_sig":sigIssuer}

		self.inputs  = [addr]
		self.outputs = [addr]


	def getBody(self):
		return self.body

	def getInputs(self):
		return self.inputs

	def getOutputs(self):
		return self.outputs

	def getId(self):
		return 11
