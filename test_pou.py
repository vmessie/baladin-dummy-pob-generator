from new_client import NewClientTransaction
from new_relay  import NewRelayTransaction
from new_path   import NewPathTransaction
from new_pou	import PoUTransaction
from del_path	import DelPathTransaction

from transaction import BsonTransaction

from Crypto.PublicKey import RSA

from random import randint

from sys import argv

# Récupération de l'endpoint

if (len(argv) > 1):endpoint=argv[1]
else:endpoint="http://127.0.0.1:8008"

# On crée des paires de clés pour l'émetteur / le récepteur
keyEMIT = RSA.generate(2048)
keyRCVS = RSA.generate(2048)

# On les enregistre sur le ledger
ncte = NewClientTransaction(keyEMIT)
ncte.bufferize()

nctr = NewClientTransaction(keyRCVS)
nctr.bufferize()

# Nombre aléatoire de relais : entre 2 et 10 relais
nbRelays = randint(2, 10)

print("Nombre de relais à créer : " + str(nbRelays))

keyRLYS = []

# On crée / enregistre sur le registre le nombre de relais requis
for k in range(0, nbRelays):
	keyRLYS.append(RSA.generate(2048))

	nrt = NewRelayTransaction(keyRLYS[-1])
	nrt.bufferize()

# On envoie la transaction de création du circuit
npt = NewPathTransaction(keyEMIT, keyRLYS, keyRCVS)
npt.bufferize()

BsonTransaction.sendTx(endpoint)

# On y ajoute 4 PoU
pou1 = PoUTransaction(1, 100, 			keyEMIT, keyRLYS, keyRCVS)
pou2 = PoUTransaction(2, 1000000,		keyEMIT, keyRLYS, keyRCVS)
pou3 = PoUTransaction(3, 50			, 	keyEMIT, keyRLYS, keyRCVS)
pou4 = PoUTransaction(4, 1000		, 	keyEMIT, keyRLYS, keyRCVS)

pou1.bufferize()
pou2.bufferize()
pou3.bufferize()
pou4.bufferize()

# On envoie le batch précédemment créé
BsonTransaction.sendTx(endpoint)

input("avant la suppression")

# suppression du circuit

dpt = DelPathTransaction(pou1.pou.pathId, keyEMIT)
dpt.bufferize()

BsonTransaction.sendTx(endpoint)

input("avant le test")

# On réessaye d'envoyer une Proof of Use sur le circuit (doit générer une erreur)
pou5 = PoUTransaction(5, 745455545454, keyEMIT, keyRLYS, keyRCVS)
pou5.bufferize()

BsonTransaction.sendTx(endpoint)
