#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from transaction import GLOBAL_NAMESPACE,BsonTransaction
from Crypto.Signature import pkcs1_15 as Signer
from Crypto.Hash	  import SHA256

import binascii

class DelPathTransaction(BsonTransaction):
	def __init__(self, pathId, author):
		"""Send a del path transaction

		Parameters
		----------
		pathId : bytes
			The ID of the path to delete (compliant to specification)
		author : RSA key
			The key of the issuer of the request (should be within a path)
			The whole key is needed, not only the public
		"""
		BsonTransaction.__init__(self)

		authorPK = author.publickey().exportKey('DER')
		pathId = pathId

		s = Signer.new(author)

		h = SHA256.new()
		h.update(pathId + authorPK)

		sigAuthor = s.sign(h)

		pathAddr = GLOBAL_NAMESPACE + "03" + str(binascii.hexlify(pathId).decode("ascii")) + "ffffffffffffffffffffffffffffff"
		authAddr = GLOBAL_NAMESPACE + "01" + str(binascii.hexlify(authorPK[-31:]).decode("ascii"))

		self.body = {"path_id":pathId,"author_key":authorPK,"author_sig":sigAuthor}

		self.inputs  = [pathAddr, authAddr]
		self.outputs = [pathAddr]


	def getBody(self):
		return self.body

	def getInputs(self):
		return self.inputs

	def getOutputs(self):
		return self.outputs

	def getId(self):
		return 13
