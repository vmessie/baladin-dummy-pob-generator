#!/usr/bin/env python3
# -*- coding: utf-8 -*-


GLOBAL_NAMESPACE="313433"


from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15 as Signer

import bson
import binascii
import base64

from transaction import GLOBAL_NAMESPACE,BsonTransaction
from pou import FakePoU

from time import time

import binascii


padding = 1000

class PoUTransaction(BsonTransaction):
	def __init__(self, number, amount, keyEmitter, keyRelays, keyReceiver):
		"""Send a Proof of Use transaction (to an existing path)

		Parameters
		----------
		number : int
			The PoU number
		amount : int
			The data amount in the Pou
		keyEmitter : RSA key
			The emitter key instance (the whole key is needed, not only the public)
		keyRelays : list
			The list of relays keys (the whole keys are needed, not only the publics)
		keyReceiver : RSA key
			The receiver key instance (the whole key is needed, not only the public)
		"""
		BsonTransaction.__init__(self)

		self.pou = FakePoU(keyEmitter, keyReceiver, keyRelays, number, int(time()), amount, padding, isFirst = False)
		self.pou.calculate()

		pathId = self.pou.pathId


		emitterPk 	= keyEmitter.publickey().exportKey('DER')
		receiverPk	= keyReceiver.publickey().exportKey('DER')
		relaysPk	= []

		for k in keyRelays:
			relaysPk.append(k.publickey().exportKey('DER'))

		addr = GLOBAL_NAMESPACE + "03" + str(binascii.hexlify(self.pou.pathId).decode("ascii")) + "ffffffffffffffffffffffffffffff"
		print(addr)



		self.body = {"emitter_key":emitterPk,"receiver_key":receiverPk,"relay_keys":relaysPk, "pou": self.pou.toBsonArray()}

		self.inputs  = [addr]
		self.outputs = [addr]


	def getBody(self):
		return self.body

	def getInputs(self):
		return self.inputs

	def getOutputs(self):
		return self.outputs

	def getId(self):
		return 10
