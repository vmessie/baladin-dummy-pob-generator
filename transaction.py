GLOBAL_NAMESPACE="313433"

import urllib.request
from urllib.error import HTTPError
from threading import Thread
import sys

from sawtooth_signing import create_context
from sawtooth_signing import CryptoFactory

from sawtooth_sdk.protobuf.transaction_pb2 import TransactionHeader
from sawtooth_sdk.protobuf.transaction_pb2 import Transaction

from sawtooth_sdk.protobuf.batch_pb2 import BatchHeader
from sawtooth_sdk.protobuf.batch_pb2 import Batch
from sawtooth_sdk.protobuf.batch_pb2 import BatchList


import sawtooth_sdk
import bson

from random import random,randint

from Crypto.Hash import SHA512


waitingTx = []

context = create_context('secp256k1')
private_key = context.new_random_private_key()
signer = CryptoFactory(context).new_signer(private_key)




# Super classe

class BsonTransaction():
	"""Abstract class for sending BLORK transactions in BSON format

	Parameters
	----------
	family : string
		Transaction family (default : "blork")
	version : string (defaut)
		Transaction processor version (default : "1.0")
	dependencies : list
		Transaction dependencies if required (default [])

	"""
	def __init__(self, family="blork", version="1.1", dependencies = []):
			self.family   = family
			self.version  = version
			self.deps	  = dependencies

			context = create_context('secp256k1')
			private_key = context.new_random_private_key()
			signer = CryptoFactory(context).new_signer(private_key)

	def getBody(self):
		"""Abstract method

		Returns
		-------
		dict
			The transaction payload

		"""
		raise Exception("Abstract method not implemented")

	def getInputs(self):
		"""Abstract method

		Returns
		-------
		list
			The inputs of the transaction (if required)

		"""

		raise Exception("Abstract method not implemented")

	def getOutputs(self):
		"""Abstract method

		Returns
		-------
		list
			The outputs of the transaction (if required)

		"""
		raise Exception("Abstract method not implemented")

	def getId(self):
		"""Abstract method

		Returns
		-------
		int
			The transaction ID

		"""
		raise Exception("Abstract method not implemented")

	def encodePayload(self, id, body):
		""" Encode the transaction payload into a BSON formatted bytes collection
		Parameters
		----------
		id : int
			The transaction ID
		body : dict
			The transaction body to encode

		Returns
		-------
		bytes
			The encoded transaction

		"""
		finalP = {"id" : id, "body" : body, "token":random()}
		return bson.BSON.encode(finalP)

	def bufferize(self):
		"""Prepare the current transaction instance, and add it to the buffer
		"""
		payload = self.encodePayload(self.getId(), self.getBody())

		payloadHash = SHA512.new()
		payloadHash.update(payload)

		txn_header_bytes = TransactionHeader(
			signer_public_key=signer.get_public_key().as_hex(),
			family_name=self.family,
			family_version=self.version,
			inputs=self.getInputs(),
			outputs=self.getOutputs(),
			# In this example, we're signing the batch with the same private key,
			# but the batch can be signed by another party, in which case, the

			# In this example, there are no dependencies.  This list should include
			# an previous transaction header signatures that must be applied for
			# this transaction to successfully commit.
			# For example,
			# dependencies=['540a6803971d1880ec73a96cb97815a95d374cbad5d865925e5aa0432fcf1931539afe10310c122c5eaae15df61236079abbf4f258889359c4d175516934484a'],
			dependencies=self.deps,
			payload_sha512=payloadHash.hexdigest(),
			batcher_public_key=signer.get_public_key().as_hex(),
			nonce=hex(randint(0, 2**64))
		).SerializeToString()

		self.sig = signer.sign(txn_header_bytes)

		txn = Transaction(
			header=txn_header_bytes,
			header_signature=self.sig,
			payload=payload
		)

		waitingTx.append(txn)

		print("TX ID : " + txn.header_signature)

	def getSignature(self):
		"""Get the transaction signature

		Returns
		-------
		string
			The transaction signature

		"""

		return self.sig

	def sendTx(endpoint="http://127.0.0.1:8008"):
		"""
		Packs the whole buffer into a batch, and send it to the validator

		Parameters
		----------
		endpoint : string
			The REST API URL (defaut http://127.0.0.1:8008)
		"""
		global waitingTx

		print("Clé : " + signer.get_public_key().as_hex())

		batch_header_bytes = BatchHeader(
		signer_public_key=signer.get_public_key().as_hex(),
			transaction_ids=[txn.header_signature for txn in waitingTx],
		).SerializeToString()

		signature = signer.sign(batch_header_bytes)

		batch = Batch(
			header=batch_header_bytes,
			header_signature=signature,
			transactions=waitingTx,
			trace=True
		)

		batch_list_bytes = BatchList(batches=[batch]).SerializeToString()

		request = urllib.request.Request(
			endpoint + "/batches",
			batch_list_bytes,
			method='POST',
			headers={'Content-Type': 'application/octet-stream'})

		t = TxHandler(request)
		t.start()

		waitingTx = []



class TxHandler(Thread):
	def __init__(self, req):
		Thread.__init__(self)
		self.request = req
	def run(self):
		try:
			response= urllib.request.urlopen(self.request)
		except HTTPError as e:
			response = e.file
			print("Error " + str(response.status) + " : " + response.msg)
			sys.exit(42)
