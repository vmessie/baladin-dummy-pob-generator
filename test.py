from new_client import NewClientTransaction
from del_client import DelClientTransaction
from new_relay  import NewRelayTransaction
from del_relay  import DelRelayTransaction


from transaction import BsonTransaction

from Crypto.PublicKey import RSA

# Génération d'une paire de clés
key2Add = RSA.generate(2048)
# Transaction pour un nouveau client
nct = NewClientTransaction(key2Add)
nct.bufferize()
BsonTransaction.sendTx()

print("on retente la création")
# On retente la création du nouveau client (doit renvoyer une erreur)
nct2 = NewClientTransaction(key2Add)
nct2.bufferize()
BsonTransaction.sendTx()
# suppression du client récemment ajouté
dct = DelClientTransaction(key2Add)
dct.bufferize()

BsonTransaction.sendTx()

input("enter pour recréer")
# On le recrée (après suppression)
nct3 = NewClientTransaction(key2Add)
nct3.bufferize()

BsonTransaction.sendTx()

# Génération d'une paire de clé pour un nouveau relais
key2Add = RSA.generate(2048)
# Transaction pour nouveau relais
nrt = NewRelayTransaction(key2Add)
nrt.bufferize()

BsonTransaction.sendTx()
# On tente de recréer le relais (doit générer une erreur).
print("on retente la création")

nrt2 = NewRelayTransaction(key2Add)
nrt2.bufferize()

BsonTransaction.sendTx()

# Suppression du relais précédemment créé

drt = DelRelayTransaction(key2Add)
drt.bufferize()

BsonTransaction.sendTx()

input("enter pour recréer")

# re-création du relais précédemment supprimé

nrt3 = NewRelayTransaction(key2Add)
nrt3.bufferize()
BsonTransaction.sendTx()
